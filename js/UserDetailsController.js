/**
 * Created by brett on 10/12/14.
 */
///<reference path="./UserService.ts"/>
var UserDetailsController = (function () {
    function UserDetailsController($scope, UserService) {
        var _this = this;
        this.onUserLogin = function (event, user) {
            console.log("A User logged in");
            _this.userIsLoggedIn = true;
        };
        this.onUserLogout = function () {
            console.log("A user logged out");
            _this.userIsLoggedIn = false;
        };
        this.isUserLoggedIn = function () {
            return _this.userIsLoggedIn;
        };
        var currentUser = UserService.getCurrentUser();
        if (currentUser) {
            this.userIsLoggedIn = true;
        }
        else {
            this.userIsLoggedIn = false;
        }
        $scope.isUserLoggedIn = this.isUserLoggedIn;
        $scope.$on(UserServiceEvents.USER_LOGIN_EVENT, this.onUserLogin);
        $scope.$on(UserServiceEvents.USER_LOGOUT_EVENT, this.onUserLogout);
    }
    return UserDetailsController;
})();
//# sourceMappingURL=UserDetailsController.js.map
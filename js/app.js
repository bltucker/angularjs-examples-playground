/**
 * Created by brett on 10/10/14.
 */

var app = angular.module("myApp", ['ngRoute', 'ngToast']);



app.config(['$routeProvider', function($routeProvider){

    $routeProvider.when("/", {
        controller: "HomeController",
        templateUrl: "./home.html"
    });


    $routeProvider.when("/Home",{
        controller: "HomeController",
        templateUrl: "./home.html"
    });

    $routeProvider.when("/UserDetails", {
        controller: "UserDetailsController",
        templateUrl: "./userdetails.html"
    });
}]);

app.directive('userBadge', function(){
    return {
        templateUrl : './userbadge.html',
        restrict: 'E'
    };
});
app.controller('UserBadgeController', ['$scope','UserService', UserBadge]);

app.service('UserService', ["$rootScope", UserService]);


app.controller("HomeController",['$scope', 'ngToast', HomeController]);
app.controller("MenuController", ['$scope', 'UserService', MenuController]);
app.controller("UserDetailsController", ['$scope', 'UserService', UserDetailsController]);

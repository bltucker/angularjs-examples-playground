/**
 * Created by brett on 10/11/14.
 */
var User = (function () {
    function User(firstName, lastName) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.firstName = firstName;
        this.lastName = lastName;
    }
    return User;
})();
var UserServiceEvents = (function () {
    function UserServiceEvents() {
    }
    UserServiceEvents.USER_LOGIN_EVENT = "userLogin";
    UserServiceEvents.USER_LOGOUT_EVENT = "userLogout";
    return UserServiceEvents;
})();
var UserService = (function () {
    function UserService($rootScope) {
        this.currentUser = null;
        this.eventBus = $rootScope;
    }
    UserService.prototype.login = function (aUser) {
        console.log("AUser", aUser);
        this.currentUser = aUser;
        this.eventBus.$broadcast(UserServiceEvents.USER_LOGIN_EVENT, aUser);
    };
    UserService.prototype.logout = function () {
        this.currentUser = null;
        this.eventBus.$broadcast(UserServiceEvents.USER_LOGOUT_EVENT);
    };
    UserService.prototype.getCurrentUser = function () {
        return this.currentUser;
    };
    return UserService;
})();
//# sourceMappingURL=UserService.js.map
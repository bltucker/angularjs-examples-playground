/**
 * Created by brett on 10/12/14.
 */
///<reference path="./UserService.ts"/>



class UserBadge {

    public userFirstName: string;
    public userLastName: string;

    private userService: UserService;

    constructor($scope,UserService){
        $scope.$on(UserServiceEvents.USER_LOGIN_EVENT, this.onUserLogin);

        this.userFirstName = "";
        this.userLastName = "";
      //  $scope.firstname = this.userFirstName;
      //  $scope.lastname = this.userLastName;
       // $scope.logout = this.logout;
        this.userService = UserService;
    }


    public onUserLogin = (event, user) => {
        this.userFirstName = user.firstName;
        this.userLastName = user.lastName;
    }


    public getUserFirstName = () =>{
        return this.userFirstName;
    }


    public getUserLastName = () => {
        return this.userLastName;
    }

    public logout(){
        console.log("logout");
        this.userService.logout();
    }
}
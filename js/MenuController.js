/**
 * Created by brett on 10/11/14.
 */
///<reference path="./UserService.ts"/>
var MenuItem = (function () {
    function MenuItem(view, title) {
        this.view = view;
        this.title = title;
        this.view = view;
        this.title = title;
    }
    return MenuItem;
})();
var MenuController = (function () {
    function MenuController($scope, UserService) {
        var _this = this;
        this.getMenuItems = function () {
            return _this.menuItems;
        };
        this.onUserLogin = function (event, user) {
            _this.userIsLoggedIn = true;
        };
        this.onUserLogout = function () {
            _this.userIsLoggedIn = false;
        };
        this.menuItems = new Array();
        this.menuItems.push(new MenuItem("Home", "Home"));
        this.menuItems.push(new MenuItem("UserDetails", "User Details"));
        this.userIsLoggedIn = false;
        $scope.$on(UserServiceEvents.USER_LOGIN_EVENT, this.onUserLogin);
        $scope.$on(UserServiceEvents.USER_LOGOUT_EVENT, this.onUserLogout);
        this.userService = UserService;
    }
    MenuController.prototype.login = function (firstName, lastName) {
        console.log("loggin the user in");
        console.log("this: ", this);
        console.log(this.userService);
        this.userService.login(new User(firstName, lastName));
    };
    return MenuController;
})();
//# sourceMappingURL=MenuController.js.map
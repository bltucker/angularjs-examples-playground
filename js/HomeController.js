/**
 * Created by brett on 10/10/14.
 */
///<reference path="./UserService.ts"/>
var HomeController = (function () {
    /*
    $broadcast sends an event down the scope chain
    $emit sends an event up the scope chain
    $on listens for a scope event

    see more in module 0506
     */
    function HomeController($scope, ngToast) {
        var _this = this;
        this.onUserLogin = function (event, user) {
            if (!user.firstName || !user.lastName || user.firstName.length == 0 || user.lastName.length == 0) {
                _this.ngToastService.create({ content: "Invalid user!", class: "danger" });
                return;
            }
            _this.ngToastService.create(user.firstName + " " + user.lastName + " has logged in!");
        };
        this.ngToastService = ngToast;
        $scope.$on(UserServiceEvents.USER_LOGIN_EVENT, this.onUserLogin);
    }
    return HomeController;
})();
//# sourceMappingURL=HomeController.js.map
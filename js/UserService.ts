/**
 * Created by brett on 10/11/14.
 */

class User{
    constructor(public firstName:string, public lastName:string){
        this.firstName = firstName;
        this.lastName = lastName;
    }
}

class UserServiceEvents{
    public static USER_LOGIN_EVENT = "userLogin";
    public static USER_LOGOUT_EVENT = "userLogout";
}


class UserService{



    private currentUser: User;
    private eventBus;

    constructor($rootScope){
        this.currentUser = null;
        this.eventBus = $rootScope;
    }


    public login(aUser:User) : void{
        console.log("AUser", aUser);
        this.currentUser = aUser;
        this.eventBus.$broadcast(UserServiceEvents.USER_LOGIN_EVENT, aUser);
    }


    public logout(): void{
        this.currentUser = null;
        this.eventBus.$broadcast(UserServiceEvents.USER_LOGOUT_EVENT);
    }


    public getCurrentUser(){
        return this.currentUser;
    }
}
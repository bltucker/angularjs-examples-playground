/**
 * Created by brett on 10/11/14.
 */

///<reference path="./UserService.ts"/>

class MenuItem{

    constructor(public view:string, public title:string){
        this.view = view;
        this.title = title;
    }
}


class MenuController{

    public menuItems: MenuItem[];
    public userService: UserService;
    public userIsLoggedIn: boolean;

    public firstName: string;
    public lastName: string;

    constructor($scope, UserService: UserService){

        this.menuItems = new Array();
        this.menuItems.push(new MenuItem("Home", "Home"));
        this.menuItems.push(new MenuItem("UserDetails", "User Details"));

        this.userIsLoggedIn = false;

        $scope.$on(UserServiceEvents.USER_LOGIN_EVENT, this.onUserLogin);
        $scope.$on(UserServiceEvents.USER_LOGOUT_EVENT, this.onUserLogout);

        this.userService = UserService;
    }


    public getMenuItems = () => {
        return this.menuItems;
    }

    public login(firstName, lastName) {
        console.log("loggin the user in");
        console.log("this: ", this);
        console.log(this.userService);
        this.userService.login(new User(firstName, lastName));
    }

    public onUserLogin = (event, user) => {
        this.userIsLoggedIn = true;
    }

    public onUserLogout = () => {
        this.userIsLoggedIn = false;
    }
}
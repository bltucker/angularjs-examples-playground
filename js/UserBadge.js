/**
 * Created by brett on 10/12/14.
 */
///<reference path="./UserService.ts"/>
var UserBadge = (function () {
    function UserBadge($scope, UserService) {
        var _this = this;
        this.onUserLogin = function (event, user) {
            _this.userFirstName = user.firstName;
            _this.userLastName = user.lastName;
        };
        this.getUserFirstName = function () {
            return _this.userFirstName;
        };
        this.getUserLastName = function () {
            return _this.userLastName;
        };
        $scope.$on(UserServiceEvents.USER_LOGIN_EVENT, this.onUserLogin);
        this.userFirstName = "";
        this.userLastName = "";
        //  $scope.firstname = this.userFirstName;
        //  $scope.lastname = this.userLastName;
        // $scope.logout = this.logout;
        this.userService = UserService;
    }
    UserBadge.prototype.logout = function () {
        console.log("logout");
        this.userService.logout();
    };
    return UserBadge;
})();
//# sourceMappingURL=UserBadge.js.map
/**
 * Created by brett on 10/10/14.
 */

///<reference path="./UserService.ts"/>


class HomeController{

    private ngToastService;

    /*
    $broadcast sends an event down the scope chain
    $emit sends an event up the scope chain
    $on listens for a scope event

    see more in module 0506
     */
    constructor($scope, ngToast){
        this.ngToastService = ngToast;
        $scope.$on(UserServiceEvents.USER_LOGIN_EVENT, this.onUserLogin);
    }


    private onUserLogin = (event, user) => {
        if(!user.firstName || !user.lastName|| user.firstName.length == 0 || user.lastName.length == 0){
            this.ngToastService.create({ content : "Invalid user!", class :  "danger"});
            return;
        }

        this.ngToastService.create(user.firstName + " " + user.lastName + " has logged in!");
    }
}
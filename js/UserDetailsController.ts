
/**
 * Created by brett on 10/12/14.
 */


///<reference path="./UserService.ts"/>


class UserDetailsController{

    private userIsLoggedIn :boolean;

    constructor($scope, UserService){

        var currentUser = UserService.getCurrentUser();

        if(currentUser){
            this.userIsLoggedIn = true;
        } else {
            this.userIsLoggedIn = false;
        }

        $scope.isUserLoggedIn = this.isUserLoggedIn;

        $scope.$on(UserServiceEvents.USER_LOGIN_EVENT, this.onUserLogin);
        $scope.$on(UserServiceEvents.USER_LOGOUT_EVENT, this.onUserLogout);
    }

    public onUserLogin = (event, user) => {
        console.log("A User logged in");
        this.userIsLoggedIn = true;
    }

    public onUserLogout = () => {
        console.log("A user logged out");
        this.userIsLoggedIn = false;
    }


    public isUserLoggedIn = () => {
        return this.userIsLoggedIn;
    }
}